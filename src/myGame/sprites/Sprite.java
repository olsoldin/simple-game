package myGame.sprites;

public class Sprite{

    public final int SIZE;
    private int x, y;
    public int[] pixels;
    private SpriteSheet sheet;
    //--//
    public static Sprite grass = new Sprite(16, 0, 1, SpriteSheet.tiles);
    public static Sprite flower = new Sprite(16, 1, 0, SpriteSheet.tiles);
    public static Sprite rock = new Sprite(16, 2, 0, SpriteSheet.tiles);
    public static Sprite voidSprite = new Sprite(16, 0x1B87E0); //blue
    //--//
    //spawn level sprites
    public static Sprite spawnGrass = new Sprite(16, 0, 0, SpriteSheet.spawnLevel);
    public static Sprite spawnHedge = new Sprite(16, 1, 0, SpriteSheet.spawnLevel);
    public static Sprite spawnWater = new Sprite(16, 2, 0, SpriteSheet.spawnLevel);
    public static Sprite spawnWall1 = new Sprite(16, 0, 1, SpriteSheet.spawnLevel);
    public static Sprite spawnWall2 = new Sprite(16, 0, 2, SpriteSheet.spawnLevel);
    public static Sprite spawnFloor = new Sprite(16, 1, 1, SpriteSheet.spawnLevel);
    //--//
    //player sprites
    public static Sprite playerF = new Sprite(32, 0, 7, SpriteSheet.tiles);
    public static Sprite playerF1 = new Sprite(32, 1, 7, SpriteSheet.tiles);
    public static Sprite playerF2 = new Sprite(32, 3, 7, SpriteSheet.tiles);
    //--//
    public static Sprite playerB = new Sprite(32, 0, 4, SpriteSheet.tiles);
    public static Sprite playerB1 = new Sprite(32, 1, 4, SpriteSheet.tiles);
    public static Sprite playerB2 = new Sprite(32, 3, 4, SpriteSheet.tiles);
    //--//
    public static Sprite playerL = new Sprite(32, 0, 5, SpriteSheet.tiles);
    public static Sprite playerL1 = new Sprite(32, 1, 5, SpriteSheet.tiles);
    public static Sprite playerL2 = new Sprite(32, 3, 5, SpriteSheet.tiles);
    //--//
    public static Sprite playerR = new Sprite(32, 0, 6, SpriteSheet.tiles);
    public static Sprite playerR1 = new Sprite(32, 1, 6, SpriteSheet.tiles);
    public static Sprite playerR2 = new Sprite(32, 3, 6, SpriteSheet.tiles);

    public Sprite(int size, int x, int y, SpriteSheet sheet){
        SIZE = size;
        pixels = new int[SIZE * SIZE];
        this.x = x * size;
        this.y = y * size;
        this.sheet = sheet;
        load();
    }

    public Sprite(int size, int colour){
        SIZE = size;
        pixels = new int[SIZE * SIZE];
        setColour(colour);
    }

    public void setColour(int colour){
        for(int i = 0; i < SIZE * SIZE; i++){
            pixels[i] = colour;
        }
    }

    private void load(){
        for(int $y = 0; $y < SIZE; $y++){
            for(int $x = 0; $x < SIZE; $x++){
                pixels[$x + $y * SIZE] =
                sheet.pixels[($x + this.x) + ($y + this.y) * sheet.SIZE];
            }
        }
    }
}