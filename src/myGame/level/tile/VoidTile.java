package myGame.level.tile;

import myGame.Screen;
import myGame.level.tile.Tile;
import myGame.sprites.Sprite;
import myGame.sprites.Sprite;


public class VoidTile extends Tile {

    public VoidTile(Sprite sprite){
        super(sprite);
    }

    @Override
    public void render(int x, int y, Screen screen){
        screen.renderTile(x << 4, y << 4, this);
    }
}