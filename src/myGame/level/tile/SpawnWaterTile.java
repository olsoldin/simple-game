/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myGame.level.tile;

import myGame.Screen;
import myGame.sprites.Sprite;

/**
 *
 * @author Oliver
 */
class SpawnWaterTile extends Tile{

    public SpawnWaterTile(Sprite sprite){
        super(sprite);
    }

    @Override
    public void render(int x, int y, Screen screen){
        screen.renderTile(x << 4, y << 4, this);
    }
}
