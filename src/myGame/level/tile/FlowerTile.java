package myGame.level.tile;

import myGame.Screen;
import myGame.sprites.Sprite;

/**
 *
 * @author Oliver
 */
public class FlowerTile extends Tile{

    public FlowerTile(Sprite sprite){
        super(sprite);
    }

    @Override
    public void render(int x, int y, Screen screen){
        screen.renderTile(x << 4, y << 4, this);
    }
}
