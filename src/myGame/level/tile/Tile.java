package myGame.level.tile;

import myGame.Screen;
import myGame.sprites.Sprite;

public class Tile{

    public int x, y;
    public Sprite sprite;
    //--//
    public static Tile grass = new GrassTile(Sprite.grass);
    public static Tile flower = new FlowerTile(Sprite.flower);
    public static Tile rock = new RockTile(Sprite.rock);
    public static Tile voidTile = new VoidTile(Sprite.voidSprite);
    //--//
    public static Tile spawnGrass = new SpawnGrassTile(Sprite.spawnGrass);
    public static Tile spawnHedge = new SpawnHedgeTile(Sprite.spawnHedge);
    public static Tile spawnWater = new SpawnWaterTile(Sprite.spawnWater);
    public static Tile spawnWall1 = new SpawnWallTile(Sprite.spawnWall1);
    public static Tile spawnWall2 = new SpawnWallTile(Sprite.spawnWall2);
    public static Tile spawnFloor = new SpawnFloorTile(Sprite.spawnFloor);
    //--//
    public static final int GRASS = 0xFF00FF00;
    public static final int FLOWER = 0xFFFFFF00;
    public static final int ROCK = 0xFF7F7F00;
    //--//
    public static final int SPAWN_HEDGE = 0; //unused
    public static final int SPAWN_WATER = 1; //unused
    public static final int SPAWN_WALL1 = 0xFF808080;
    public static final int SPAWN_WALL2 = 0xFF000000;
    public static final int SPAWN_FLOOR = 0xFF2D1C00;

    public Tile(Sprite sprite){
        this.sprite = sprite;
    }

    public void render(int x, int y, Screen screen){
        screen.renderTile(x << 4, y << 4, this);
    }

    public boolean solid(){
        return false;
    }

    public static Tile getTile(int tileCol){
        switch(tileCol){
            case GRASS:
                return spawnGrass;
            case SPAWN_HEDGE:
                return spawnHedge;
            case SPAWN_WATER:
                return spawnWater;
            case SPAWN_WALL1:
                return spawnWall1;
            case SPAWN_WALL2:
                return spawnWall2;
            case SPAWN_FLOOR:
                return spawnFloor;
        }
        return voidTile;
    }
}