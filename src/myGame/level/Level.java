package myGame.level;

import myGame.Screen;
import myGame.level.tile.Tile;

public class Level{

    protected int width, height;
    protected int[] tilesInt;
    protected int[] tiles;
    //--//
    public static Level spawn = new SpawnLevel("/assets/levels/spawn.png");

    public Level(int width, int height){
        this.width = width;
        this.height = height;
        tilesInt = new int[width * height];
        generateLevel();
    }

    public Level(String path){
        loadLevel(path);
        generateLevel();
    }

    //if randomlevel is specified, whenever generateLevel is run (including
    //  inside level) it will run it for randomLevel
    protected void generateLevel(){
    }

    protected void loadLevel(String path){
    }

    public void update(){
    }

    private void time(){
    }

    public void render(int xScroll, int yScroll, Screen screen){
        screen.setOffset(xScroll, yScroll);
        int x0 = xScroll >> 4; //xScroll / 16 (16 is size of tiles)
        int x1 = (xScroll + screen.width + 16) >> 4;
        int y0 = yScroll >> 4; //xScroll / 16
        int y1 = (yScroll + screen.height + 15) >> 4;

        for(int y = y0; y < y1; y++){
            for(int x = x0; x < x1; x++){
                getTile(x, y).render(x, y, screen);
            }
        }
    }

    public Tile getTile(int x, int y){
        if(x < 0 || y < 0 || x >= width || y >= height){
            return Tile.voidTile;
        }
        return Tile.getTile(tiles[x + y * width]);
    }
}