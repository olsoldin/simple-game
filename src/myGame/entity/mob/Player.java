package myGame.entity.mob;

import myGame.Screen;
import myGame.sprites.Sprite;
import myGame.utils.Keyboard;

public class Player extends Mob{

    private Keyboard input;
    private Sprite sprite;
    private int anim = 0;
    private boolean walking = false;

    public Player(Keyboard input){
        this.input = input;
        sprite = Sprite.playerF;
    }

    public Player(int x, int y, Keyboard input){
        this(input);
        this.x = x;
        this.y = y;
    }

    @Override
    public void update(){
        int xa = 0, ya = 0;
        if(anim++ > 7500){
            anim = 0;
        }
        if(input.up){
            ya--;
        }
        if(input.down){
            ya++;
        }
        if(input.left){
            xa--;
        }
        if(input.right){
            xa++;
        }
        if(xa != 0 || ya != 0){
            move(xa, ya);
            walking = true;
        }else{
            walking = false;
        }
    }

    @Override
    public void render(Screen screen){
        switch(dir){
            case 0:
                sprite = Sprite.playerF;
                if(walking){
                    if(anim % 20 > 10){
                        sprite = Sprite.playerF1;
                    }else{
                        sprite = Sprite.playerF2;
                    }
                }
                break;
            case 1:
                sprite = Sprite.playerR;
                if(walking){
                    if(anim % 20 > 10){
                        sprite = Sprite.playerR1;
                    }else{
                        sprite = Sprite.playerR2;
                    }
                }
                break;
            case 2:
                sprite = Sprite.playerB;
                if(walking){
                    if(anim % 20 > 10){
                        sprite = Sprite.playerB1;
                    }else{
                        sprite = Sprite.playerB2;
                    }
                }
                break;
            case 3:
                sprite = Sprite.playerL;
                if(walking){
                    if(anim % 20 > 10){
                        sprite = Sprite.playerL1;
                    }else{
                        sprite = Sprite.playerL2;
                    }
                }
                break;
        }
        screen.renderPlayer(x - 16, y - 16, sprite);
    }
}